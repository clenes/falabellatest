//
//  AlamofireService.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import Foundation
import Alamofire

class AlamofireService {
 
    static func get(at route: String, params: Parameters = [:]) -> DataRequest {
        return request(at: route, method: .get, params: params, encoding: URLEncoding.default)
    }
    
    static func request(at route: String, method: HTTPMethod, params: Parameters = [:], encoding: ParameterEncoding) -> DataRequest {
        let routeUrl = "\(requestDomain)\(route)"
        return AF.request(routeUrl, method: method, parameters: params, encoding: encoding, headers: nil)
            .validate()
    }
}
