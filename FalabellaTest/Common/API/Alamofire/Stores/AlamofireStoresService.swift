//
//  AlamofireStoresService.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import Foundation


class AlamofireStoresService: AlamofireService, StoresService {
    
    
    static func getStoresService(completion: @escaping (Bool, [APIStore]?, Error?) -> Void) {
        get(at: StoresServiceRouter.stores.path).responseJSON { (response) in
            print(response)
            switch response.result {
            case .success:
                if response.response?.statusCode != 200 {
                    completion(false, nil, nil)
                } else {
                    do {
                        let dataStores = try JSONDecoder().decode([APIStore].self, from: response.data!)
                        completion(true, dataStores, nil)
                    } catch let decodingError {
                        completion(false, nil, decodingError)
                    }
                }
                
                // es para saber si hubo un error a la hora de hacer el llamado
            case .failure(let error):
                do{
                    //here dataResponse received from a network request
                    let jsonResponse = try JSONSerialization.jsonObject(with: response.data!, options: [])
                    print(jsonResponse) //Response result
                    completion(false, nil, nil)
                } catch let parsingError {
                    print("Error", parsingError)
                    completion(false, nil, parsingError)
                }
            }
        }
    }
}
