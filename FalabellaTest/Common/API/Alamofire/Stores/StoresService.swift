//
//  StoresService.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import Foundation

protocol StoresService: class {
    
    typealias storesResponse = (Bool, [APIStore]?, Error?) -> Void
    
    static func getStoresService(completion: @escaping StoresService.storesResponse)
}
