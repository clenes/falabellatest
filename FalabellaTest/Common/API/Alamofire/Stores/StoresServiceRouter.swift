//
//  StoresServiceRouter.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import Foundation


enum StoresServiceRouter: String {

    case stores
    
    var path: String {
        switch self {
        /// Login Router
        case .stores:
            return "getLocales"
        }
    }
}
