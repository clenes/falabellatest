//
//  Stores.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import Foundation

// MARK: - APIStore
struct APIStore: Codable {
    var fecha, localID, localNombre, comunaNombre: String?
    var localidadNombre, localDireccion, funcionamientoHoraApertura, funcionamientoHoraCierre: String?
    var localTelefono, localLat, localLng, funcionamientoDia: String?
    var fkRegion, fkComuna, fkLocalidad: String?

    enum CodingKeys: String, CodingKey {
        case fecha
        case localID = "local_id"
        case localNombre = "local_nombre"
        case comunaNombre = "comuna_nombre"
        case localidadNombre = "localidad_nombre"
        case localDireccion = "local_direccion"
        case funcionamientoHoraApertura = "funcionamiento_hora_apertura"
        case funcionamientoHoraCierre = "funcionamiento_hora_cierre"
        case localTelefono = "local_telefono"
        case localLat = "local_lat"
        case localLng = "local_lng"
        case funcionamientoDia = "funcionamiento_dia"
        case fkRegion = "fk_region"
        case fkComuna = "fk_comuna"
        case fkLocalidad = "fk_localidad"
    }
}
