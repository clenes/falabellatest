//
//  LoginPresenter.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 11/05/21.
//

import Foundation
import CommonCrypto

final class LoginPresenter {
        
    let key256   = "12345678901234561234567890123456"   // 32 bytes for AES256
    let iv       = "abcdefghijklmnop"                   // 16 bytes for AES128
    var aes256: AES!
    
    private unowned let view: LoginViewController
    
    // MARK: - Lifecycle -

    init(_ view: LoginViewController) {
        self.view = view
        aes256 = AES(key: key256, iv: iv)
        setLoginData()
    }
    
    private func setLoginData() {
        let email = encryptData(stringToEncrypt: "carolainlenes@gmail.com")
        let pass = encryptData(stringToEncrypt: "1234")
        
        UserDefaultHelper.saveUserDefault(UserInformationKey.email.key, email, nil)
        UserDefaultHelper.saveUserDefault(UserInformationKey.password.key, pass, nil)
    }
    
    private func encryptData(stringToEncrypt: String) -> String {
        guard let encryptData = aes256.encrypt(string: stringToEncrypt) else {
            return ""
        }
        return String(decoding: encryptData, as: UTF8.self)
    }
    
    func decryptData(stringEncrypt: String) -> String{
        let dataEncrypt = Data(stringEncrypt.utf8)
        guard let decryptedData = aes256.decrypt(data: dataEncrypt) else {
            return ""
            
        }
        return decryptedData
    }
    
    func enterAccountPresenter(email: String, password: String) {
        self.view.hiddeIndicator()
        if email.isEmpty || password.isEmpty {
            Utils.showSnackbar("user.empty.fields.login.description.label".localized, 5.0)
            return
        }

        if !email.isValidEmail {
            Utils.showSnackbar("user.error.email.description.label".localized, 5.0)
            return
        }

        let emailUserDefault = UserDefaultHelper.getUserDefault(UserInformationKey.email.key).stringValue ?? ""
        
        let passUserDefault = UserDefaultHelper.getUserDefault(UserInformationKey.password.key).stringValue ?? ""
        
        if email == emailUserDefault, password == passUserDefault{
            UserDefaultHelper.saveUserDefault(UserInformationKey.login.key, nil, true)
            self.view.openStores()
        }else{
            Utils.showSnackbar("user.invalid.fields".localized, 5.0)
        }
    }
    
}
