//
//  LoginViewController.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 11/05/21.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: - IBOutlets -
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    // MARK: - Public properties -
    var presenter: LoginPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        loginConfigurerView()
        // Do any additional setup after loading the view.
    }
    
    private func loginConfigurerView() {
        let login = UserDefaultHelper.getUserDefault(UserInformationKey.login.key).booleanValue ?? false
        if login {
            self.openStores()
        }
        
        self.presenter = LoginPresenter(self)
        UserDefaultHelper.saveUserDefault(UserInformationKey.email.key, "carolainlenes@gmail.com", nil)
        UserDefaultHelper.saveUserDefault(UserInformationKey.password.key, "1234", nil)
    }
    

    @IBAction func enterAccountAction(_ sender: Any) {
        self.showIndicator()
        self.presenter.enterAccountPresenter(email: emailTextField.text ?? "", password: passwordTextField.text ?? "")
    }
    

}

extension LoginViewController {
    func showIndicator() {
        Utils.showLoading(self.view)
    }

    func hiddeIndicator() {
        Utils.hiddenLoading(self.view)
    }
    
    func openStores(){
        
        let storyBoard : UIStoryboard = UIStoryboard( name : "Stores" , bundle: Bundle.main )
        let storesViewController = storyBoard.instantiateViewController( withIdentifier : "StoresViewController" ) as! StoresViewController
        
        self.navigationController?.pushViewController(storesViewController, animated: true)
    }
}
