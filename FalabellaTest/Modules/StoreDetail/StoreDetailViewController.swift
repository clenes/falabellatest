//
//  StoreDetailViewController.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 11/05/21.
//

import UIKit

class StoreDetailViewController: UIViewController {
    
    // MARK: - IBOutlets -
    @IBOutlet weak var localNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var regionNameLabel: UILabel!
    @IBOutlet weak var communeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dayOperationLabel: UILabel!
    @IBOutlet weak var hourOpenLabel: UILabel!
    @IBOutlet weak var hourCloseLabel: UILabel!
    @IBOutlet weak var communenameLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var longLabel: UILabel!
    
    
    // MARK: - Variables -
    var storeData: APIStore!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        configurerView()
      
    }
    
    func configurerView() {
        self.localNameLabel.text = storeData.localNombre
        self.dateLabel.text = storeData.fecha
        self.regionNameLabel.text = storeData.fkRegion
        self.communeLabel.text = storeData.fkComuna
        self.locationLabel.text = storeData.fkLocalidad
        self.dayOperationLabel.text = "Funcionamiento día: \( storeData.funcionamientoDia ?? "")"
        self.hourOpenLabel.text = storeData.funcionamientoHoraApertura
        self.hourCloseLabel.text = storeData.funcionamientoHoraCierre
        self.communenameLabel.text = "Comuna: \(storeData.comunaNombre ?? "")"
        self.locationNameLabel.text = "Localidad: \(storeData.localidadNombre ?? "")"
        self.addressLabel.text = "Dirección: \(storeData.localDireccion ?? "")"
        self.phoneLabel.text = "Teléfono: \(storeData.localTelefono ?? "")"
        self.latLabel.text = "Latitud: \(storeData.localLat ?? "")"
        self.longLabel.text = "Longitud: \(storeData.localLng ?? "")"
    }
    
    // MARK: - IBAction -
    @IBAction func backAction() {
        self.navigationController?.popViewController(animated: true)
    }


}
