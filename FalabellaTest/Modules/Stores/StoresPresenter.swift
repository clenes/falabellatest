//
//  StoresPresenter.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 11/05/21.
//

import Foundation

final class StoresPresenter {
    
    
    // MARK: - Private properties -
    private unowned let view: StoresViewController
    
    // MARK: - Lifecycle -
    init(_ view: StoresViewController){
        self.view = view
    }
    
    func presenter(){
        getStores()
    }
    
    func getStores(){
        self.view.showIndicator()
        AlamofireStoresService.getStoresService { (result, data, error) in
            if let apiStore = data, result && error == nil  {
                self.view.hiddeIndicator()
                self.view.updateData(arrayStores: apiStore)
            }else {
                self.view.showNoDataView()
            }
        }
    }
}
