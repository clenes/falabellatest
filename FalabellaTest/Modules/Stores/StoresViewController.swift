//
//  StoresViewController.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 11/05/21.
//

import UIKit

class StoresViewController: UIViewController {
    
    // MARK: - IBOutlets -
    @IBOutlet weak var storesTablewView: UITableView!
    @IBOutlet weak var showView: UIView!
    @IBOutlet weak var userEmail: UILabel!
    
    //MARK: - Variables -
    var arrayStores: [APIStore] = [] {
        didSet {
            storesTablewView.reloadData()
            
        }
    }
    
    // MARK: - Public properties -
    var presenter: StoresPresenter!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = StoresPresenter(self)
        self.presenter.getStores()
        userEmail.text = "Hola: \( UserDefaultHelper.getUserDefault(UserInformationKey.email.key).stringValue ?? "")"
    }
    
    // MARK: - IBAction -
    @IBAction func logoutAction() {
        
        UserDefaultHelper.saveUserDefault(UserInformationKey.login.key, nil, false)
        
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension StoresViewController {
    
    func showIndicator() {
        Utils.showLoading(self.view)
    }

    func hiddeIndicator() {
        Utils.hiddenLoading(self.view)
    }
    
    func updateData(arrayStores: [APIStore]) {
        self.arrayStores = arrayStores
    }
    
    func showNoDataView() {
        self.showView.isHidden = false
    }
    
    func openDetailStore(_ indexPath: Int ){
        
        let storyBoard : UIStoryboard = UIStoryboard( name : "StoreDetail" , bundle: Bundle.main )
        let storeDetailViewController = storyBoard.instantiateViewController( withIdentifier : "StoreDetailViewController" ) as! StoreDetailViewController
    
        storeDetailViewController.storeData = arrayStores[indexPath]
        self.navigationController?.pushViewController(storeDetailViewController, animated: true)
    }
    
}

extension StoresViewController: UITableViewDelegate,UITableViewDataSource{

    //MARK: TABLEVIEW DELEGATE

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayStores.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell( withIdentifier : "StoresTableViewCell" , for : indexPath ) as! StoresTableViewCell
        cell.loadStoreData(self.arrayStores[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.openDetailStore(indexPath.row)
        
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

    func tableView(_ tableView: UITableView,heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 150
    }

}
