//
//  StoresTableViewCell.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import UIKit

class StoresTableViewCell: UITableViewCell {


    //MARK: - IBOutlets -
    @IBOutlet weak var localNameLabel: UILabel!
    @IBOutlet weak var localAddressLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    
    //MARK: - Variables -
    var index: Int = 0
    var storeRow: APIStore!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func loadStoreData(_ storeRow: APIStore) {
        self.storeRow = storeRow

        self.localNameLabel.text = "Nombre: \(storeRow.localNombre ?? "-")"
        self.localAddressLabel.text = "Nombre: \(storeRow.localDireccion ?? "-")"
        self.locationNameLabel.text = "Nombre: \(storeRow.localidadNombre ?? "-")"
    
    }

}
