//
//  UserInformationKey.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import Foundation

enum UserInformationKey: String {
    case email = "email"
    case password = "password"
    case login = "login"
    
    var key: String {
        return self.rawValue
    }
}
