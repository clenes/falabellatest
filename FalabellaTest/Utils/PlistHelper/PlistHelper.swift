//
//  PlistHelper.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import Foundation

struct PlistHelper {

    //sacar la info del infoplis con el nombre del key
    static func getInfoPlistKey(key: String) -> String {
        return (Bundle.main.infoDictionary?[key] as? String)?
            .replacingOccurrences(of: "\\", with: "") ?? ""
    }
}
