//
//  UserDefaultHelper.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import Foundation


struct UserDefaultHelper {

    static func saveUserDefault(_ key: String, _ stringValue: String?, _ booleanValue: Bool?) {
        let userDefault = UserDefaults.standard
        if stringValue != nil {
            userDefault.set(stringValue, forKey: key)
        } else {
            userDefault.set(booleanValue, forKey: key)
        }
    }
    static func getUserDefault(_ key: String) -> (stringValue: String?, booleanValue: Bool?) {
        let userDefault = UserDefaults.standard
        if key == UserInformationKey.login.key {
            return (nil, userDefault.object(forKey: key) as? Bool)
        }
        return (userDefault.object(forKey: key) as? String, nil)
    }

    static func removeDefault() {
        let userDefault = UserDefaults.standard
        userDefault.removeObject(forKey: UserInformationKey.email.key)
        userDefault.removeObject(forKey: UserInformationKey.password.key)
    }
}
