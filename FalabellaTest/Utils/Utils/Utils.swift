//
//  Utils.swift
//  FalabellaTest
//
//  Created by Carolain Lenes Beltran on 12/05/21.
//

import Foundation
import MBProgressHUD
import MaterialComponents.MaterialSnackbar


open class Utils {
    
    
    // Show Load Indicator
    static func showLoading(_ view: UIView) {
        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        hud.bezelView.color = UIColor(named: "black")
        hud.bezelView.backgroundColor = UIColor(named: "white")
    }
    
    static func hiddenLoading(_ view: UIView) {
        MBProgressHUD.hide(for: view, animated: true)
    }
    
    //-------------- Alert Snackbar --------------!!
    static func showSnackbar (_ fieldName: String, _ showerTime: Double) {
        let message = MDCSnackbarMessage()
        message.duration = showerTime
        message.text = NSLocalizedString(fieldName, comment: "utils.snackbarComment".localized)
        MDCSnackbarManager.default.show(message)
    }
    
    
}
