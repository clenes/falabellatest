//
//  FalabellaTestTests.swift
//  FalabellaTestTests
//
//  Created by Carolain Lenes Beltran on 11/05/21.
//

import XCTest
@testable import FalabellaTest

class FalabellaTestTests: XCTestCase {
    
    let userEmail = "carolainlenes@gmail.com"
    let userPass = "1234"

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testUserLoginNotNil() {
        let login = UserDefaultHelper.getUserDefault(UserInformationKey.login.key).booleanValue ?? false
        XCTAssertNotNil(login)
    }
    
    func testUserEmail() {
        let email = UserDefaultHelper.getUserDefault(UserInformationKey.email.key).stringValue ?? ""
        
        XCTAssertEqual(email, userEmail)
    }
    
    func testUserPassword() {
        let password = UserDefaultHelper.getUserDefault(UserInformationKey.password.key).stringValue ?? ""
        XCTAssertEqual(password, password)
    }

}
